package com.company;

public enum OpcoesSlot {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300);

    private int value;

    public static OpcoesSlot random() {
        return OpcoesSlot.values()[Double.valueOf(
                Math.random() * OpcoesSlot.values().length).intValue()];
    }

    OpcoesSlot(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }
}

