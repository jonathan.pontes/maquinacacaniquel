package com.company;

public class MaquinaCacaNiquel {

    public void jogar() {
        //Caso repetirem 3 simbolos igual a pontuação é multiplicada por 100
        //Cada execução do programa do programa deve sortear a maquina uma vez e exibir o resultado
        //A maquina deve exibir no console o valor de cada slot e a pontuação final

        Slot slot = criarListaComOpcoesSlot();
        boolean saoIguais = verificarIgualdade(slot);
        calcularPontuacaoFinal(slot, saoIguais);
    }

    private Slot criarListaComOpcoesSlot() {
        Slot slot = new Slot();

        int quantidadeDeSlots = 3;
        for (int i = 0; i < quantidadeDeSlots; i++) {
            slot.slots.add(OpcoesSlot.random());
        }
        return slot;
    }


    private boolean verificarIgualdade(Slot slot) {
        boolean saoIguais = false;

        for (int i = 0; i < slot.slots.size(); i++) {
            for (int j = 1; j < slot.slots.size(); j++) {

                OpcoesSlot valor1 = slot.slots.get(i);
                OpcoesSlot valor2 = slot.slots.get(j);

                if (valor1.equals(valor2)) {
                    saoIguais = true;
                } else {
                    return saoIguais = false;
                }
            }
        }
        return saoIguais;
    }

    private void calcularPontuacaoFinal(Slot slot, boolean saoIguais) {

        int soma = 0;
        for (int i = 0; i < slot.slots.size(); i++) {
            int posicaoSlot = i + 1;

            System.out.println("Valor do Slot " + posicaoSlot + ": " +
                    slot.slots.get(i).toString() + " -> " +
                    slot.slots.get(i).getValue());

            soma += slot.slots.get(i).getValue();
        }

        if (saoIguais) {
            soma = soma * 100;
        }
        System.out.println("\nPontuação final: " + soma);
    }
}
