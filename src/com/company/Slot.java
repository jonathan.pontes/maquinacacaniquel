package com.company;

import java.util.ArrayList;
import java.util.List;

public class Slot {
    List<OpcoesSlot> slots = new ArrayList<>();

    public List<OpcoesSlot> getSlots() {
        return slots;
    }

    public void setSlots(List<OpcoesSlot> slots) {
        this.slots = slots;
    }
}
